@extends('main')

@section('title', 'Laravel - SI Perpustakaan')

@section('content')
<div class="container">
        <div class="jumbotron">
            <h1 class="display-6">Tambah Data Buku</h1>
            <hr class="my-4">
            <div class="card-body">
                    
                @if ($msg = Session::get('msg'))
                    <div class="alert alert-success">
                        <span>{{ $msg }}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>   
                    </div>
                @endif    

            <form action="{{ route('buku.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" name="judul" placeholder="Judul Buku">
                </div>
                <div class="form-group">
                    <label for="penulis">Deskripsi</label>
                    <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi">
                </div>
                <div class="form-group">
                    <label for="penerbit">Kategori</label>
                    <input type="text" class="form-control" name="kategori" placeholder="Kategori">
                </div>
                <div class="form-group">
                    <label for="tahun_terbit">Penulis</label>
                    <input class="form-control" name="pengarang" placeholder="Penulis">
                </div>
                <div class="form-group">
                    <label for="kategori">Tahun</label>
                    <input type="text" class="form-control" name="tahun" placeholder="Tahun Terbit">
                </div>
                <div class="form-group">
                    <label for="kategori">Jumlah Buku</label>
                    <input type="text" class="form-control" name="stok" placeholder="Jumlah Buku">
                </div>
                <div class="form-group">
                    <label for="kategori">Cover Buku</label>
                    <input type="file" class="form-control" name="cover" placeholder="Cover Buku">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
    </div>
@endsection