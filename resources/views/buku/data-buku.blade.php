@extends('main')

@section('title', 'Laravel - SI Perpustakaan')

@section('content')
    <div class="container">
        <div class="jumbotron">

            @php
                $judul = ['Codingan', 'Beuty Inside', "SEMUA"];
            @endphp


            @if ($msg = Session::get('msg'))
                <div class="alert alert-success">
                    <span>{{ $msg }}</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>   
                </div>
            @endif 

            <h1 class="display-6">Data Buku</h1>
            <hr class="my-2">     

            <a href="{{ route('buku.create') }}" class="btn btn-primary mb-1 my-3">Tambah Buku</a>
            <form action="{{route('buku.index')}}" class="row mb-4">
                    <div class="col-md-8">
                        <select name="nama" class="form-control">
                            <option value="" disabled selected>Pilih Judul Buku</option>
                            @foreach ($judul as $jdl)
                            <option value="{{ $jdl == "SEMUA" ? "" : $jdl }}" >{{ $jdl }} </option>
                            @endforeach 
                        </select>
                    </div>
                    <input type="submit" class="btn btn-success float-right mb-4" value="Cari">

            <table class="table">
                <thead class="thead-dark">
                    <tr>
                
                    <th scope="col">Judul Buku</th>
                    <th scope="col">Deskripsi</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Penulis</th>
                    <th scope="col">Tahun Terbit</th>
                    <th scope="col">Jumlah Buku</th>
                    <th scope="col">Cover Buku</th>
                    <th scope="col">Action</th>
                    
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dataBuku as $bk)
                    <tr>
                        <td>{{ $bk['judul'] }}</td>
                        <td>{{ $bk['deskripsi'] }}</td>
                        <td>{{ $bk['kategori'] }}</td>
                        <td>{{ $bk['pengarang'] }}</td>
                        <td>{{ $bk['tahun'] }}</td>
                        <td>{{ $bk['stok'] }}</td>
                        <td><img src='image/{{ $bk->cover }}' style='width:80px; height:100px;'></td>

                        <td>
                            <form action="{{ route('buku.destroy',$bk['id']) }}" method="POST">
                            <a href="{{ route('buku.show',$bk['id']) }}" class="badge badge-primary">Detail</a>
                            <a href="{{ route('buku.edit',$bk['id']) }}" class="badge badge-primary">Edit</a>
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="badge badge-danger" onclick="return confirm('Yakin ingin menghapus data?')">Hapus</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script>
        function action(){
            var judul = document.getElementById('judul').value;
            window.location.href = "{{ url('buku') }}/"+judul;
        }
    </script>
@endsection