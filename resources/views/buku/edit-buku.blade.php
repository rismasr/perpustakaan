@extends('main')

@section('title', 'Laravel - SI Perpustakaan')

@section('content')
<div class="container">
        <div class="jumbotron">
            <h1 class="display-6">Edit Data Buku</h1>
            <hr class="my-4">
            <div class="card-body">

             @if ($dataBuku)
            <form action="{{ route('buku.update', $dataBuku['id']) }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" name="judul" placeholder="Judul Buku" value="{{ $dataBuku['judul'] }}">
                </div>
                <div class="form-group">
                    <label for="penulis">Deskripsi</label>
                    <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi" value="{{ $dataBuku['deskripsi'] }}">
                </div>
                <div class="form-group">
                    <label for="penerbit">Kategori</label>
                    <input type="text" class="form-control" name="kategori" placeholder="Kategori" value="{{ $dataBuku['kategori'] }}">
                </div>
                <div class="form-group">
                    <label for="tahun_terbit">Penulis</label>
                    <input class="form-control" name="pengarang" placeholder="Penulis" value="{{ $dataBuku['pengarang'] }}">
                </div>
                <div class="form-group">
                    <label for="kategori">Tahun</label>
                    <input type="text" class="form-control" name="tahun" placeholder="Tahun Terbit" value="{{ $dataBuku['tahun'] }}">
                </div>
                <div class="form-group">
                    <label for="kategori">Jumlah Buku</label>
                    <input type="text" class="form-control" name="stok" placeholder="Jumlah Buku" value="{{ $dataBuku['stok'] }}">
                </div>
                <div class="form-group">
                    <label for="kategori">Cover Buku</label>
                    <input type="file" class="form-control" name="cover" placeholder="Cover Buku" value="{{ $dataBuku['cover'] }}">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
            @else
                <p>Data tidak jelas, tidak dapat mengedit data</p>
            @endif
        </div>
    </div>
</div>
@endsection