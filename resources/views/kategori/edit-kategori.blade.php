@extends('main')

@section('title', 'Laravel - SI Perpustakaan')

@section('content')
<div class="container">
        <div class="jumbotron">
            <h1 class="display-6">Edit Data Kategori</h1>
            <hr class="my-4">
            <div class="card-body">

                @if ($msg = Session::get('msg'))
                    <div class="alert alert-success">
                        <span>{{ $msg }}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>   
                    </div>
                @endif

             @if ($dataKategori)
            <form action="{{ route('kategori.update', $dataKategori['id']) }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="nama">Nama Kategori</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama Kategori" value="{{ $dataKategori['nama'] }}">
                </div>
                <div class="form-group">
                    <label for="alamat">Deskripsi</label>
                    <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi Kategori" value="{{ $dataKategori['deskripsi'] }}">
                </div>
                
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
            @else
                <p>Data tidak jelas, tidak dapat mengedit data</p>
            @endif
        </div>
    </div>
</div>
@endsection