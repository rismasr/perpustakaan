<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dataBuku = Buku::all();


        if($request->query('judul')){
            $dataBuku = Buku::where('judul', request()->judul)->get();
        }

        return view('buku.data-buku', compact('dataBuku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('buku.create-buku');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:50',
            'deskripsi' => 'required|max:255',
            'kategori' => 'required|max:30',
            'pengarang' => 'required|max:30',
            'tahun' => 'required|max:4',
            'stok' => 'required|max:3',
            'cover' => 'required|max:255',
         
        ]);
        Buku::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'kategori' => $request->kategori,
            'pengarang' => $request->pengarang,
            'tahun' => $request->tahun,
            'stok' => $request->stok,
            'cover' => $request->cover,
        ]);
        
        return redirect()->route('buku.index')->with('msg', 'Data anda telah diinputkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataBuku = Buku::find($id);
        return view('buku.detail-buku', compact('dataBuku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataBuku = Buku::find($id);
        return view('buku.edit-buku',compact('dataBuku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:50',
            'deskripsi' => 'required|max:255',
            'kategori' => 'required|max:30',
            'pengarang' => 'required|max:30',
            'tahun' => 'required|max:4',
            'stok' => 'required|max:3',
            'cover' => 'required|max:255',
         
        ]);

        Buku::whereId($id)->update($validasi);
        return redirect()->route('buku.index')->with('msg', 'Data anda telah diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::findOrFail($id);
        $buku->delete();
        return redirect()->route('buku.index')->with('msg', 'Data anda telah dihapus!');
    }
}
